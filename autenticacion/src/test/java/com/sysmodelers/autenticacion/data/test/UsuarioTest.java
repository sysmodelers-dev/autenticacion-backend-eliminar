package com.sysmodelers.autenticacion.data.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sysmodelers.autenticacion.data.model.Usuario;
import com.sysmodelers.autenticacion.security.util.Security;

public class UsuarioTest extends JPAHibernateTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save() throws Exception {
		em.getTransaction().begin();
		List<Usuario> listUsuario = null;
		int maxUsuario = 0;
		try {
			maxUsuario = (int) em.createNativeQuery("select max(id_usuario) from autenticacion.usuario ").getSingleResult();
			System.out.println("Max Usuario :" + maxUsuario);

			// System.out.println("Resultado "+result);
			Usuario usuario = new Usuario();
			usuario.setEmail("test" + (maxUsuario + 1) + "@gmail.com");
			usuario.setNombre("Nombre Usuario-" + (maxUsuario + 1));
			usuario.setPassword(encryptText("123"));

			em.persist(usuario);
			em.getTransaction().commit();

			listUsuario = new ArrayList<Usuario>();
			listUsuario = em.createNativeQuery("select *  from Usuario", Usuario.class).getResultList();

			System.out.println("*********************************************************************");
			for (Usuario usuarioL : listUsuario) {
				System.out.println(usuarioL.getNombre() + "  " + " " + usuarioL.getEmail());
			}
			System.out.println("*********************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(listUsuario);
		System.out.println("Registros nuevos :" + listUsuario.size() + " vs " + " Registros anteriores :" + maxUsuario);
		assertTrue(listUsuario.size() > maxUsuario);
	}

	private String encryptText(String value) {
		try {

			return Security.encript(value);

		} catch (Exception e) {
			e.printStackTrace();
			return value;
		}
	}

	@Test
	public void get() throws Exception {

		Usuario usuario = em.find(Usuario.class, 1);
		System.out.println("*********************************************************************");
		System.out.println(usuario.getNombre() + "  " + " " + usuario.getEmail() + " " + encryptText("1"));
		System.out.println("*********************************************************************");
		assertNotNull(usuario);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listUsuario() {
		List<Usuario> listUsuario = new ArrayList<Usuario>();
		listUsuario = em.createNativeQuery("select *  from autenticacion.usuario order by id_usuario", Usuario.class).getResultList();
		System.out.println("*********************************************************************");
		for (Usuario usuarioL : listUsuario) {
			System.out.println(usuarioL.getNombre() + "  " + " " + usuarioL.getEmail());
		}
		System.out.println("*********************************************************************");
		assertTrue(listUsuario.size() > 0);

	}
}
