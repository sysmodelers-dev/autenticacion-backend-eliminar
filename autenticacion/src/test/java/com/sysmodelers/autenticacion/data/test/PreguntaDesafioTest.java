package com.sysmodelers.autenticacion.data.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sysmodelers.autenticacion.data.model.PreguntaDesafio;

public class PreguntaDesafioTest extends JPAHibernateTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save() throws Exception {
		em.getTransaction().begin();
		List<PreguntaDesafio> listPreguntaDesafio = null;
		int maxPreguntaDesafio = 0;
		try {
			maxPreguntaDesafio = (int) em
					.createNativeQuery("select max(id_pregunta) from autenticacion.pregunta_desafio ")
					.getSingleResult();
			System.out.println("Max PreguntaDesafio :" + maxPreguntaDesafio);

			// System.out.println("Resultado "+result);
			PreguntaDesafio preguntaDesafio = new PreguntaDesafio();
			preguntaDesafio.setDescripcion("Descripcion PreguntaDesafio-" + (maxPreguntaDesafio + 1));

			em.persist(preguntaDesafio);
			em.getTransaction().commit();

			listPreguntaDesafio = new ArrayList<PreguntaDesafio>();
			listPreguntaDesafio = em
					.createNativeQuery("select *  from autenticacion.pregunta_desafio order by id_pregunta", PreguntaDesafio.class)
					.getResultList();

			System.out.println("*********************************************************************");
			for (PreguntaDesafio preguntaDesafioL : listPreguntaDesafio) {
				System.out.println(preguntaDesafioL.getIdPregunta() + " " + preguntaDesafioL.getDescripcion());
			}
			System.out.println("*********************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(listPreguntaDesafio);
		System.out.println("Registros nuevos :" + listPreguntaDesafio.size() + " vs " + " Registros anteriores :"
				+ maxPreguntaDesafio);
		assertTrue(listPreguntaDesafio.size() > maxPreguntaDesafio);
	}

	@Test
	public void get() throws Exception {

		PreguntaDesafio preguntaDesafio = em.find(PreguntaDesafio.class, 1);
		System.out.println("*********************************************************************");
		System.out.println(preguntaDesafio.getIdPregunta() + " " + preguntaDesafio.getDescripcion());
		System.out.println("*********************************************************************");
		assertNotNull(preguntaDesafio);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listPreguntaDesafio() {
		List<PreguntaDesafio> listPreguntaDesafio = new ArrayList<PreguntaDesafio>();
		listPreguntaDesafio = em
				.createNativeQuery("select *  from autenticacion.pregunta_desafio order by id_pregunta",
						PreguntaDesafio.class)
				.getResultList();
		System.out.println("*********************************************************************");
		for (PreguntaDesafio preguntaDesafioL : listPreguntaDesafio) {
			System.out.println(preguntaDesafioL.getIdPregunta() + " " + preguntaDesafioL.getDescripcion());
		}
		System.out.println("*********************************************************************");
		assertTrue(listPreguntaDesafio.size() > 0);

	}
}
