package com.sysmodelers.autenticacion.data.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sysmodelers.autenticacion.data.model.Permiso;
import com.sysmodelers.autenticacion.data.model.Sistema;

public class PermisoTest extends JPAHibernateTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save() throws Exception {
		em.getTransaction().begin();
		List<Permiso> listPermiso = null;
		int maxPermiso = 0;
		try {
			maxPermiso = (int) em.createNativeQuery("select max(id_permiso) from autenticacion.permiso ")
					.getSingleResult();
			System.out.println("Max Permiso :" + maxPermiso);

			// System.out.println("Resultado "+result);
			Permiso permiso = new Permiso();
			permiso.setDescripcion("Descripcion Permiso-" + (maxPermiso + 1));
			permiso.setNombreCorto("NombreCorto Permiso-" + (maxPermiso + 1));

			Sistema sistema = new Sistema();
			sistema.setIdSistema(1);
			permiso.setSistema(sistema);

			em.persist(permiso);
			em.getTransaction().commit();

			listPermiso = new ArrayList<Permiso>();
			listPermiso = em.createNativeQuery("select *  from autenticacion.permiso", Permiso.class).getResultList();

			System.out.println("*********************************************************************");
			for (Permiso permisoL : listPermiso) {
				System.out.println(permisoL.getIdPermiso() + " " + permisoL.getSistema().getDescripcion() + " "
						+ permisoL.getDescripcion());
			}
			System.out.println("*********************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(listPermiso);
		System.out.println("Registros nuevos :" + listPermiso.size() + " vs " + " Registros anteriores :" + maxPermiso);
		assertTrue(listPermiso.size() > maxPermiso);
	}

	@Test
	public void get() throws Exception {

		Permiso permiso = em.find(Permiso.class, 1);
		System.out.println("*********************************************************************");
		System.out.println(
				permiso.getIdPermiso() + " " + permiso.getSistema().getDescripcion() + " " + permiso.getDescripcion());
		System.out.println("*********************************************************************");
		assertNotNull(permiso);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listPermiso() {
		List<Permiso> listPermiso = new ArrayList<Permiso>();
		listPermiso = em.createNativeQuery("select *  from autenticacion.permiso order by id_permiso", Permiso.class)
				.getResultList();
		System.out.println("*********************************************************************");
		for (Permiso permisoL : listPermiso) {
			System.out.println(permisoL.getIdPermiso() + " " + permisoL.getSistema().getDescripcion() + " "
					+ permisoL.getDescripcion());
		}
		System.out.println("*********************************************************************");
		assertTrue(listPermiso.size() > 0);

	}
}
