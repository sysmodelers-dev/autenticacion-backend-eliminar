package com.sysmodelers.autenticacion.data.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sysmodelers.autenticacion.data.model.Sistema;

public class SistemaTest extends JPAHibernateTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save() throws Exception {
		em.getTransaction().begin();
		List<Sistema> listSistema = null;
		int maxSistema = 0;
		try {
			maxSistema = (int) em.createNativeQuery("select max(id_sistema) from autenticacion.sistema ")
					.getSingleResult();
			System.out.println("Max Sistema :" + maxSistema);

			// System.out.println("Resultado "+result);
			Sistema sistema = new Sistema();
			sistema.setNombre("Nombre Sistema-" + (maxSistema + 1));
			sistema.setDescripcion("Descripcion Sistema-" + (maxSistema + 1));
			sistema.setCant_preg_desafio(2);
			sistema.setDias_cambio_clave(90);

			em.persist(sistema);
			em.getTransaction().commit();

			listSistema = new ArrayList<Sistema>();
			listSistema = em.createNativeQuery("select *  from autenticacion.sistema", Sistema.class).getResultList();

			System.out.println("*********************************************************************");
			for (Sistema sistemaL : listSistema) {
				System.out
						.println(sistemaL.getIdSistema() + " " + sistemaL.getNombre() + " " + sistemaL.getDescripcion()
								+ " " + sistemaL.getCant_preg_desafio() + " " + sistemaL.getDias_cambio_clave());
			}
			System.out.println("*********************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(listSistema);
		System.out.println("Registros nuevos :" + listSistema.size() + " vs " + " Registros anteriores :" + maxSistema);
		assertTrue(listSistema.size() > maxSistema);
	}

	@Test
	public void get() throws Exception {

		Sistema sistema = em.find(Sistema.class, 1);
		System.out.println("*********************************************************************");
		System.out.println(sistema.getIdSistema() + " " + sistema.getNombre() + " " + sistema.getDescripcion() + " "
				+ sistema.getCant_preg_desafio() + " " + sistema.getDias_cambio_clave());
		System.out.println("*********************************************************************");
		assertNotNull(sistema);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listSistema() {
		List<Sistema> listSistema = new ArrayList<Sistema>();
		listSistema = em.createNativeQuery("select *  from autenticacion.sistema order by id_sistema", Sistema.class)
				.getResultList();
		System.out.println("*********************************************************************");
		for (Sistema sistemaL : listSistema) {
			System.out.println(sistemaL.getIdSistema() + " " + sistemaL.getNombre() + " " + sistemaL.getDescripcion()
					+ " " + sistemaL.getCant_preg_desafio() + " " + sistemaL.getDias_cambio_clave());
		}
		System.out.println("*********************************************************************");
		assertTrue(listSistema.size() > 0);

	}
}
