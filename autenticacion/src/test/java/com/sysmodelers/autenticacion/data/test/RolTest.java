package com.sysmodelers.autenticacion.data.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sysmodelers.autenticacion.data.model.Rol;
import com.sysmodelers.autenticacion.data.model.Sistema;

public class RolTest extends JPAHibernateTest {

	@SuppressWarnings("unchecked")
	@Test
	public void save() throws Exception {
		em.getTransaction().begin();
		List<Rol> listRol = null;
		int maxRol = 0;
		try {
			maxRol = (int) em.createNativeQuery("select max(id_rol) from autenticacion.rol ").getSingleResult();
			System.out.println("Max Rol :" + maxRol);

			// System.out.println("Resultado "+result);
			Rol rol = new Rol();
			rol.setDescripcion("Descripcion Rol-" + (maxRol + 1));

			Sistema sistema = new Sistema();
			sistema.setIdSistema(1);
			rol.setSistema(sistema);

			em.persist(rol);
			em.getTransaction().commit();

			listRol = new ArrayList<Rol>();
			listRol = em.createNativeQuery("select *  from autenticacion.rol", Rol.class).getResultList();

			System.out.println("*********************************************************************");
			for (Rol rolL : listRol) {
				System.out.println(
						rolL.getIdRol() + " " + rolL.getSistema().getDescripcion() + " " + rolL.getDescripcion());
			}
			System.out.println("*********************************************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNotNull(listRol);
		System.out.println("Registros nuevos :" + listRol.size() + " vs " + " Registros anteriores :" + maxRol);
		assertTrue(listRol.size() > maxRol);
	}

	@Test
	public void get() throws Exception {

		Rol rol = em.find(Rol.class, 1);
		System.out.println("*********************************************************************");
		System.out.println(rol.getIdRol() + " " + rol.getSistema().getDescripcion() + " " + rol.getDescripcion());
		System.out.println("*********************************************************************");
		assertNotNull(rol);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listRol() {
		List<Rol> listRol = new ArrayList<Rol>();
		listRol = em.createNativeQuery("select *  from autenticacion.rol order by id_rol", Rol.class).getResultList();
		System.out.println("*********************************************************************");
		for (Rol rolL : listRol) {
			System.out
					.println(rolL.getIdRol() + " " + rolL.getSistema().getDescripcion() + " " + rolL.getDescripcion());
		}
		System.out.println("*********************************************************************");
		assertTrue(listRol.size() > 0);

	}
}
