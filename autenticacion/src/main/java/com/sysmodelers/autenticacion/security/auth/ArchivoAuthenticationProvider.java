/**
 * 
 */
package com.sysmodelers.autenticacion.security.auth;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author Alejandro Guzman
 *
 */
public class ArchivoAuthenticationProvider implements AuthenticationProvider,
		InitializingBean {

	/** Logger de la clase */
	private static final Logger log = Logger
			.getLogger(ArchivoAuthenticationProvider.class);

	protected MessageSourceAccessor messages = SpringSecurityMessageSource
			.getAccessor();

	/**  */
	ArchivoUserDetailServiceImpl archivoUserDetailsService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("::: Ejecutando el afterPropertiesSet()");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		log.debug("::: Ejecutando el metodo authenticate(Authentication)");
		log.debug("::: Name " + authentication.getName());
		log.debug("::: Credentials " + authentication.getCredentials());
		log.debug("::: Principal " + authentication.getPrincipal());
		
		loadUserByAssertion(authentication);
		
		return authentication;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.authentication.AuthenticationProvider#supports
	 * (java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {

		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication);
	}

	/**
	 * @param archivoUserService
	 *            the archivoUserService to set
	 */
	public void setarchivoUserDetailsService(
			final ArchivoUserDetailServiceImpl archivoUserService) {

		this.archivoUserDetailsService = archivoUserService;
	}

	
	protected UserDetails loadUserByAssertion(final Authentication authentication) {

		return this.archivoUserDetailsService.loadUserDetails(authentication);
	}

}
