package com.sysmodelers.autenticacion.security.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class AesUtil {
	
	private static final String IV_PHRASE_STRING = "12hs4234a7954mrz";
	/** */
	private static final String SALT_PHRASE_STRING = "_sA7rRv43#6000";
	/** */
	private static final int ITERATION_COUNT = 1000;
	/** */
	private static final int KEY_SIZE = 128;
	/** */
	private static final String PASS_PHRASE = "comisionfederalelectricidad2018eoficiov";

	private final int keySize;
	private final int iterationCount;
	private final Cipher cipher;

	public AesUtil(int keySize, int iterationCount) {
		this.keySize = keySize;
		this.iterationCount = iterationCount;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchPaddingException e) {
			throw fail(e);
		} catch (NoSuchAlgorithmException e) {
			throw fail(e);
		}
	}

	public String encrypt(byte[] salt, byte[] iv, String passphrase, String plaintext)
			throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		try {

			SecretKey key = generateKey(salt, passphrase);
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes("UTF-8"));
			return Hex.encodeHexString(encrypted);

			// new String(encrypted, "UTF-8"); // base64(encrypted);

		} catch (UnsupportedEncodingException e) {
			throw fail(e);
		}
	}

	// public String decrypt(String salt, String iv, String passphrase, String
	// ciphertext) {
	// try {
	// SecretKey key = generateKey(salt, passphrase);
	// byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv,
	// base64(ciphertext));
	// return new String(decrypted, "UTF-8");
	// }
	// catch (UnsupportedEncodingException e) {
	// throw fail(e);
	// }
	// }
	//
	public String decrypt(byte[] salt, byte[] iv, String passphrase, String ciphertext)
			throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, DecoderException {
		SecretKey key = generateKey(salt, passphrase);
		byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, hex(ciphertext));
		return new String(decrypted, "UTF-8");
	}

	// private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[]
	// bytes) {
	// try {
	// cipher.init(encryptMode, key, new IvParameterSpec(base64(iv)));
	// return cipher.doFinal(bytes);
	// }
	// catch (InvalidKeyException
	// | InvalidAlgorithmParameterException
	// | IllegalBlockSizeException
	// | BadPaddingException e) {
	// throw fail(e);
	// }
	// }

	private byte[] doFinal(int encryptMode, SecretKey key, byte[] iv, byte[] bytes) throws InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		cipher.init(encryptMode, key, new IvParameterSpec(iv));
		return cipher.doFinal(bytes);

	}

	/**
	 * 
	 * @param salt
	 * @param iv
	 * @param passphrase
	 * @param ciphertext
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws UnsupportedEncodingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws DecoderException
	 */
	public String decryptHex(byte[] salt, byte[] iv, String passphrase, String ciphertext)
			throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, DecoderException {
		SecretKey key = generateKey(salt, passphrase);
		byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, decodeHex(ciphertext));
		return new String(decrypted, "UTF-8");
	}
	private SecretKey generateKey(byte[] salt, String passphrase)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), salt, iterationCount, keySize);
		SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		return key;
	}

	/**
	 * 
	 * @param str
	 * @return
	 * @throws DecoderException
	 */
	public static byte[] decodeHex(String str) throws DecoderException {
		return Hex.decodeHex(str.toCharArray());
	}

	public static byte[] hex(String str) throws DecoderException {
		return Hex.decodeHex(str.toCharArray());
	}

	//
	private IllegalStateException fail(Exception e) {
		return new IllegalStateException(e);
	}

	/**
	 * @param salt
	 * @param iv
	 * @param passphrase
	 * @param ciphertext
	 * @return
	 */
	public String encryptToHex(byte[] salt, byte[] iv, String passphrase, String ciphertext)
			throws UnsupportedEncodingException, InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException, InvalidKeyException {

		SecretKey key = generateKey(salt, passphrase);
		byte[] encripted = doFinal(Cipher.ENCRYPT_MODE, key, iv, ciphertext.getBytes("UTF-8"));
		return new String(Hex.encodeHex(encripted));

	}
	
	public static void main(String[]kjlhlk){
		try {
//			String su = new AesUtil(KEY_SIZE, ITERATION_COUNT).encrypt(SALT_PHRASE_STRING.getBytes(), IV_PHRASE_STRING.getBytes(), PASS_PHRASE, "28d343c0a5e16965534c7439fef2f869");
//			String sukey = new AesUtil(KEY_SIZE, ITERATION_COUNT).encrypt(SALT_PHRASE_STRING.getBytes(), IV_PHRASE_STRING.getBytes(), PASS_PHRASE, "Archivo.2018");
//			System.out.println("su=" + su);
//			System.out.println("sukey=" + sukey);
			
			System.out.println("su=" + new AesUtil(KEY_SIZE, ITERATION_COUNT).decrypt(SALT_PHRASE_STRING.getBytes(), IV_PHRASE_STRING.getBytes(), PASS_PHRASE, "28d343c0a5e16965534c7439fef2f869"));
//			System.out.println("sukey=" + new AesUtil(KEY_SIZE, ITERATION_COUNT).decrypt(SALT_PHRASE_STRING.getBytes(), IV_PHRASE_STRING.getBytes(), PASS_PHRASE, sukey));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
