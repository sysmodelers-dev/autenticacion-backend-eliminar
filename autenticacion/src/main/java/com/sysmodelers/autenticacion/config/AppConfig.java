/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <b>Sistema Integral de Gestión y Automatización de Procesos.</b>
 * <p>
 * <b>e-oFICIO 5.0</b>
 * <p>
 * El Sistema Integral de Gestión y Automatización de Procesos (archivo) es una
 * solución abierta y modular espec�ficamente diseñada para ayudar a las
 * Instituciones de Gobierno.
 * <p>
 * <i>Consultoria y Aplicaciones Avanzadas de ECM S.A. de C.V. ECM
 * Solutions.</i>
 *
 * @author Alejandro Guzman // Adaulfo Herrera // Hugo Hernandez
 * @version 1.0
 */
@Configuration
@EnableTransactionManagement
@EnableAsync
@PropertySource("classpath:application.properties")
@PropertySource("classpath:autenticacion.properties")
public class AppConfig {
    /**
     * @param sessionFactory
     * @return
     */
    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
        return new org.springframework.orm.hibernate5.HibernateTransactionManager(sessionFactory);
    }
}
