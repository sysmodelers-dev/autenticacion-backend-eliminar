/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller.util;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LikeExpression;
import org.hibernate.criterion.MatchMode;

/**
 * 
 * Clase para el manejo de caracteres de Escape en los String.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */

public class EscapedLikeRestrictions {
	private EscapedLikeRestrictions() {
	}

	public static Criterion like(String propertyName, String value, MatchMode matchMode) {
		return like(propertyName, value, matchMode, false);
	}

	public static Criterion ilike(String propertyName, String value, MatchMode matchMode) {
		return like(propertyName, value, matchMode, true);
	}

	private static Criterion like(String propertyName, String value, MatchMode matchMode, boolean ignoreCase) {
		return new LikeExpression(propertyName, escape(value), matchMode, '!', ignoreCase) {

			private static final long serialVersionUID = -8478519355655408492L;
			/* a trick to call protected constructor */};
	}

	private static String escape(String value) {
		return value.replace("!", "!!").replace("%", "!%").replace("_", "!_");
	}
}
