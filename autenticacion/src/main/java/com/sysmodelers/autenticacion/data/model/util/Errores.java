/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones Avanzadas de ECM, S.A. de C.V. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model.util;

/**
 * Tipos de origen
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */

public enum Errores {

	REGISTRO_YA_EXISTE("El registro que se quiere agragar ya existe"), //
	UNIQUE_KEY_YA_EXISTE("Clave única que se quiere agragar ya existe"), //
	DATOS_ENTRADA_INCOMPLETOS("Faltan datos de entrada para esta peticion"), //
	ERROR_EN_BASE_DE_DATOS("Error en el repositorio");//

	/** ID. */
	private String estatus;

	/**
	 * default constructor.
	 */
	private Errores(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return estatus;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public static Errores fromVal(String estatus) {
		for (Errores temp : Errores.values()) {
			if (estatus == temp.estatus) {
				return temp;
			}
		}
		return null;
	}
}
