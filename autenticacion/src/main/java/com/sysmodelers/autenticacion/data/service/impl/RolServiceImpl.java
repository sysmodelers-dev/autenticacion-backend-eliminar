/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sysmodelers.autenticacion.data.dao.EntityDAO;
import com.sysmodelers.autenticacion.data.model.Rol;
import com.sysmodelers.autenticacion.data.service.ManagerImpl;

/**
 * Manejador en base de datos de objetos {@link Rol}.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
@Service("rolService")
public class RolServiceImpl extends ManagerImpl<Rol> {

	/** Interfaz a base de datos. */
	@Autowired
	@Qualifier("rolDao")
	public void setDao(EntityDAO<Rol> dao) {
		super.setDao(dao);
	}

}
