/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

//import com.ecm.archivo.data.audit.aspectj.Audit;
//import com.ecm.archivo.data.model.util.TipoAuditoria;

/**
 * 
 * Metodos generales de operaciones disponibles para manejo de datos hacia base
 * de datos.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
public abstract class Dao<k> implements EntityDAO<k> {

	/**
	 * 
	 */
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	/**
	 * 
	 * @return
	 */
	protected Session getSession() {

		Session session = null;

		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			e.printStackTrace();
			session = sessionFactory.openSession();
		}

		session.setCacheMode(CacheMode.NORMAL);
		session.setFlushMode(FlushMode.AUTO);

		return session;
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private Class<k> getK() {
		return (Class<k>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#isConnected()
	 */
	public String isConnected() {
		return Boolean.toString(getSession().isConnected());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ecm.sigap.data.services.EntityManager#fetch(java.io.Serializable)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public k fetch(Serializable id) {
		return (k) getSession().get(getK(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#fetchAll()
	 */
	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<k> fetchAll() {
		return getSession().createCriteria(getK()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.services.EntityManager#save(java.lang.Object)
	 */
	@Transactional
	@Override
	//@Audit(actionType = TipoAuditoria.SAVE)
	public void save(k item) {
		getSession().save(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#update(java.lang.Object)
	 */
	@Transactional
	@Override
	//@Audit(actionType = TipoAuditoria.UPDATE)
	public void update(k item) {
		getSession().update(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.services.EntityManager#delete(java.lang.Object)
	 */
	@Transactional
	@Override
	//@Audit(actionType = TipoAuditoria.DELETE)
	public void delete(k item) {
		getSession().delete(item);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.ecm.archivo.data.dao.EntityDAO#flush()
	 */
	@Override
	public void flush() {
		getSession().flush();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#execNamedQuery(java.lang.String,
	 * java.util.HashMap)
	 */
	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<k> execNamedQuery(String queryName, HashMap<String, Object> params) {

		Query query = getSession().getNamedQuery(queryName);

		setParameters(params, query);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#execUpdateQuery(java.lang.String,
	 * java.util.HashMap)
	 */
	@Transactional
	@Override
	public Integer execUpdateQuery(String queryName, HashMap<String, Object> params) {

		Query query = getSession().getNamedQuery(queryName);

		setParameters(params, query);

		return query.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#search(java.util.List,
	 * java.util.List)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<?> search(List<Criterion> restrictions, List<Order> orders, ProjectionList projections,
			Integer fetchSize, Integer firstResult) {

		Criteria criteria = createCriteria(restrictions, orders, projections, fetchSize, firstResult);

		criteria.setCacheable(true);
		criteria.setCacheMode(CacheMode.NORMAL);
		criteria.setCacheRegion("ECM_SIGAP_V_CACHE_REGION");

		if (projections == null || projections.getLength() < 1) {
			criteria.setResultTransformer(Criteria.ROOT_ENTITY);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		} else {
			criteria.setResultTransformer(Criteria.PROJECTION);
			criteria.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.dao.EntityDAO#uniqueResult(java.lang.String,
	 * java.util.HashMap)
	 */
	@Transactional
	@Override
	public Object uniqueResult(String queryName, HashMap<String, Object> params) {

		Query query = getSession().getNamedQuery(queryName);

		setParameters(params, query);

		Object uniqueResult = query.uniqueResult();

		return uniqueResult;
	}

	/**
	 * Llena un query con los parametros indicados en el Map.
	 * 
	 * @param params
	 * @param query
	 */
	protected void setParameters(HashMap<String, Object> params, Query query) {
		if (params != null)
			for (String key : params.keySet()) {
				if (params.get(key) instanceof String) {
					query.setString(key, (String) params.get(key));
				} else if (params.get(key) instanceof Integer) {
					query.setInteger(key, (Integer) params.get(key));
				} else if (params.get(key) instanceof Double) {
					query.setDouble(key, (Double) params.get(key));
				} else if (params.get(key) instanceof Date) {
					query.setDate(key, (Date) params.get(key));
				}
			}
	}

	/*
	 * 
	 */
	@Transactional
	@Override
	public Long getNextval(String seq) {

		String sql = "select " + seq + ".nextval seq from dual";

		SQLQuery sqlQuery = getSession().createSQLQuery(sql);
		sqlQuery.addScalar("seq", LongType.INSTANCE);

		return (Long) sqlQuery.uniqueResult();
	}

	/**
	 * Llena el en el query de consulta los parametros proporcionados.
	 * 
	 * @param restrictions
	 *            Condiciones WHERE
	 * @param orders
	 *            Condiciones ORDER BY
	 * @param criteria
	 *            Query de consulta.
	 * @param firstResult
	 *            Inidice Inicial de Elementos Devueltos.
	 * @param fetchSize
	 *            Tamaño de elementos devueltos.
	 */
	protected void processCriteria(Criteria criteria, List<Criterion> restrictions, List<Order> orders,
			ProjectionList projections, Integer fetchSize, Integer firstResult) {

		criteria.setFlushMode(FlushMode.ALWAYS);

		if (Objects.nonNull(firstResult) && Objects.nonNull(fetchSize))
			criteria.setFirstResult((firstResult - 1) * fetchSize);

		if (Objects.nonNull(fetchSize)) {
			criteria.setFetchSize(fetchSize);
			criteria.setMaxResults(fetchSize);
		}

		if (Objects.nonNull(restrictions))
			for (Criterion restrction : restrictions)
				criteria.add(restrction);

		if (Objects.nonNull(projections))
			criteria.setProjection(projections);

		if (Objects.nonNull(orders))
			for (Order order : orders)
				criteria.addOrder(order);

	}

	/**
	 * Definicion para consulta del objeto en especifico.
	 * 
	 * @param restrictions
	 * @param orders
	 * @param fetchSize
	 * @param firstResult
	 * @return
	 */
	abstract protected Criteria createCriteria(List<Criterion> restrictions, List<Order> orders,
			ProjectionList projections, Integer fetchSize, Integer firstResult);

}