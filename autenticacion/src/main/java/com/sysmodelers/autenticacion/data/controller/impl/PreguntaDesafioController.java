/**
* Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
*/
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sysmodelers.autenticacion.data.controller.CustomRestController;
import com.sysmodelers.autenticacion.data.controller.RESTController;
import com.sysmodelers.autenticacion.data.controller.util.EscapedLikeRestrictions;
import com.sysmodelers.autenticacion.data.model.PreguntaDesafio;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * Controladores REST para manejo de elementos tipo
 * {@link com.PreguntaDesafio.sigap.data.model.PreguntaDesafio}
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@RestController
public class PreguntaDesafioController extends CustomRestController implements RESTController<PreguntaDesafio> {

	/** Log de suscesos. */
	private static final Logger log = Logger.getLogger(PreguntaDesafioController.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#get(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/preguntaDesafio", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<PreguntaDesafio> get(
			@RequestParam(value = "id", required = true) Serializable id) {

		PreguntaDesafio item = null;
		try {

			item = mngrPreguntaDesafio.fetch(Integer.valueOf((String) id));

			log.debug(item);

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		log.debug(" Item Out >> " + item);
		return new ResponseEntity<PreguntaDesafio>(item, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#search(java.lang.Object)
	 */
	@Override
	@RequestMapping(value = "/preguntaDesafio", method = RequestMethod.POST)
	public ResponseEntity<List<?>> search(@RequestBody(required = true) PreguntaDesafio preguntaDesafio) {

		List<?> lst = new ArrayList<PreguntaDesafio>();
		log.debug("PARAMETROS DE BUSQUEDA : " + preguntaDesafio);

		try {

			// * * * * * * * * * * * * * * * * * * * * * *
			List<Criterion> restrictions = new ArrayList<Criterion>();

			if (preguntaDesafio.getIdPregunta() != null)
				restrictions.add(Restrictions.idEq(preguntaDesafio.getIdPregunta()));

			if (StringUtils.isNotBlank(preguntaDesafio.getDescripcion()))
				restrictions.add(
						EscapedLikeRestrictions.ilike("descripcion", preguntaDesafio.getDescripcion(), MatchMode.ANYWHERE));

			List<Order> orders = new ArrayList<Order>();

			orders.add(Order.asc("descripcion"));

			// * * * * * * * * * * * * * * * * * * * * * *
			lst = mngrPreguntaDesafio.search(restrictions, orders);

			log.debug("Size found >> " + lst.size());

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		return new ResponseEntity<List<?>>(lst, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#save(java.lang.Object)
	 */
	// Parametros para ser usados por Swagger
	@ApiImplicitParams({
			@ApiImplicitParam(name = "autenticacion-user-id", required = true, dataType = "string", paramType = "header") })
	@Override
	@RequestMapping(value = "/preguntaDesafio", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<PreguntaDesafio> save(@RequestBody(required = true) PreguntaDesafio preguntaDesafio)
			throws Exception {

		try {

			log.debug("PreguntaDesafio A GUARDAR >> " + preguntaDesafio);

			if (preguntaDesafio.getIdPregunta() == null) {
				mngrPreguntaDesafio.save(preguntaDesafio);
				return new ResponseEntity<PreguntaDesafio>(preguntaDesafio, HttpStatus.CREATED);
			} else {
				mngrPreguntaDesafio.update(preguntaDesafio);
				return new ResponseEntity<PreguntaDesafio>(preguntaDesafio, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ecm.sigap.data.controller.RestController#delete(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/preguntaDesafio", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@RequestParam(value = "id", required = true) Serializable id) {

		log.debug("preguntaDesafio A ELIMINAR >> " + id);

		try {
			mngrPreguntaDesafio.delete(mngrPreguntaDesafio.fetch(Integer.valueOf((String) id)));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}
}