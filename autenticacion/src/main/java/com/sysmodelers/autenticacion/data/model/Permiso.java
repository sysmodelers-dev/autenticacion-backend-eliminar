/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * Clase de entidad que representa la tabla ROLES dentro del sistema.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 */
@Entity
@Table(name = "permiso")
@SequenceGenerator(name = "seq_permiso_id_permiso_seq", sequenceName = "permiso_id_permiso_seq", allocationSize = 1)
public class Permiso implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6059915254906823541L;

	/** Identificador del Permiso. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_permiso_id_permiso_seq")
	@Column(name = "id_permiso")
	private Integer idPermiso;

	/** Sistema al que pertenece el usuario. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sistema")
	@NotFound(action = NotFoundAction.IGNORE)
	private Sistema sistema;

	/** Descripcion del Permiso. */
	@Column(name = "descripcion")
	private String descripcion;

	/** login_usuario. */
	@Column(name = "nombre_corto")
	private String nombreCorto;

	/**
	 * Gets the id permiso.
	 *
	 * @return the id permiso
	 */
	public Integer getIdPermiso() {
		return idPermiso;
	}

	/**
	 * Sets the id permiso.
	 *
	 * @param idPermiso the new id permiso
	 */
	public void setIdPermiso(Integer idPermiso) {
		this.idPermiso = idPermiso;
	}

	/**
	 * Gets the sistema.
	 *
	 * @return the sistema
	 */
	public Sistema getSistema() {
		return sistema;
	}

	/**
	 * Sets the sistema.
	 *
	 * @param sistema the new sistema
	 */
	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the nombre corto.
	 *
	 * @return the nombre corto
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * Sets the nombre corto.
	 *
	 * @param nombreCorto the new nombre corto
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

}
