/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;

import com.sysmodelers.autenticacion.data.dao.Dao;
import com.sysmodelers.autenticacion.data.model.Sistema;

/**
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
@Repository("sistemaDao")
@Transactional
public class SistemaDaoImpl extends Dao<Sistema> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sysmodelers.autenticacion.data.dao.Dao#createCriteria(java.util.List,
	 * java.util.List, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	protected Criteria createCriteria(List<Criterion> restrictions, List<Order> orders, ProjectionList projections,
			Integer fetchSize, Integer firstResult) {
		Criteria criteria = getSession().createCriteria(Sistema.class);
		
		//criteria.createAlias("rol", "rol", JoinType.LEFT_OUTER_JOIN);
		
		processCriteria(criteria, restrictions, orders, projections, fetchSize, firstResult);
		return criteria;

	}
}
