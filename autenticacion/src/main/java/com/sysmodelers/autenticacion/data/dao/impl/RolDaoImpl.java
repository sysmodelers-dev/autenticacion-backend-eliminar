/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.springframework.stereotype.Repository;

import com.sysmodelers.autenticacion.data.dao.Dao;
import com.sysmodelers.autenticacion.data.model.Rol;

/**
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
@Repository("rolDao")
@Transactional
public class RolDaoImpl extends Dao<Rol> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sysmodelers.autenticacion.data.dao.Dao#createCriteria(java.util.List,
	 * java.util.List, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	protected Criteria createCriteria(List<Criterion> restrictions, List<Order> orders, ProjectionList projections,
			Integer fetchSize, Integer firstResult) {
		Criteria criteria = getSession().createCriteria(Rol.class);
		processCriteria(criteria, restrictions, orders, projections, fetchSize, firstResult);
		return criteria;

	}
}
