/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model.util;

/**
 * Tipos de areas
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
public enum TipoActor {

	RESPONSABLE("RESPONSABLE"), //
	INFORMADOR("INFORMADOR"), //
	ELABORADOR("ELABORADOR"), //
	APROBADOR("APROBADOR");

	/** ID. */
	private String tipo;

	/**
	 * default constructor.
	 */
	private TipoActor(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {

		switch (tipo) {
		case "RESPONSABLE":
			return "RESPONSABLE";
		case "INFORMADOR":
			return "INFORMADOR";
		case "ELABORADOR":
			return "ELABORADOR";
		case "APROBADOR":
			return "APROBADOR";
		default:
			return null;
		}

	}

	/**
	 * 
	 * @param tipo
	 * @return
	 */
	public static TipoActor fromVal(String tipo) {
		for (TipoActor temp : TipoActor.values()) {
			if (tipo == temp.tipo) {
				return temp;
			}
		}
		return null;
	}

}
