/**
* Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
*/
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sysmodelers.autenticacion.data.controller.CustomRestController;
import com.sysmodelers.autenticacion.data.controller.RESTController;
import com.sysmodelers.autenticacion.data.controller.util.EscapedLikeRestrictions;
import com.sysmodelers.autenticacion.data.model.Rol;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * Controladores REST para manejo de elementos tipo
 * {@link com.Rol.sigap.data.model.Rol}
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@RestController
public class RolController extends CustomRestController implements RESTController<Rol> {

	/** Log de suscesos. */
	private static final Logger log = Logger.getLogger(RolController.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#get(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/rol", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Rol> get(@RequestParam(value = "id", required = true) Serializable id) {

		Rol item = null;
		try {

			item = mngrRol.fetch(Integer.valueOf((String) id));

			log.debug(item);

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		log.debug(" Item Out >> " + item);
		return new ResponseEntity<Rol>(item, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#search(java.lang.Object)
	 */
	@Override
	@RequestMapping(value = "/rol", method = RequestMethod.POST)
	public ResponseEntity<List<?>> search(@RequestBody(required = true) Rol rol) {

		List<?> lst = new ArrayList<Rol>();
		log.debug("PARAMETROS DE BUSQUEDA : " + rol);

		try {

			// * * * * * * * * * * * * * * * * * * * * * *
			List<Criterion> restrictions = new ArrayList<Criterion>();

			if (rol.getIdRol() != null)
				restrictions.add(Restrictions.idEq(rol.getIdRol()));

			if (StringUtils.isNotBlank(rol.getDescripcion()))
				restrictions
						.add(EscapedLikeRestrictions.ilike("descripcion", rol.getDescripcion(), MatchMode.ANYWHERE));

			if (rol.getSistema() != null)
				if (rol.getSistema().getIdSistema() != null)
					restrictions.add(Restrictions.eq("sistema.idSistema", rol.getSistema().getIdSistema()));

			List<Order> orders = new ArrayList<Order>();

			orders.add(Order.asc("descripcion"));

			// * * * * * * * * * * * * * * * * * * * * * *
			lst = mngrRol.search(restrictions, orders);

			log.debug("Size found >> " + lst.size());

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		return new ResponseEntity<List<?>>(lst, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#save(java.lang.Object)
	 */
	// Parametros para ser usados por Swagger
	@ApiImplicitParams({
			@ApiImplicitParam(name = "autenticacion-user-id", required = true, dataType = "string", paramType = "header") })
	@Override
	@RequestMapping(value = "/rol", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Rol> save(@RequestBody(required = true) Rol rol) throws Exception {

		try {

			log.debug("Rol A GUARDAR >> " + rol);

			if (rol.getIdRol() == null) {
				mngrRol.save(rol);
				return new ResponseEntity<Rol>(rol, HttpStatus.CREATED);
			} else {
				mngrRol.update(rol);
				return new ResponseEntity<Rol>(rol, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ecm.sigap.data.controller.RestController#delete(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/rol", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@RequestParam(value = "id", required = true) Serializable id) {

		log.debug("rol A ELIMINAR >> " + id);

		try {
			mngrRol.delete(mngrRol.fetch(Integer.valueOf((String) id)));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}
}