package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class PreguntaDesafio.
 */
@Entity
@Table(name = "sistema")
@SequenceGenerator(name = "seq_sistema_id_sistema_seq", sequenceName = "sistema_id_sistema_seq", allocationSize = 1)
public class Sistema implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1306567713674898325L;

	/**
	 * Identificador del usuario.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sistema_id_sistema_seq")
	@Column(name = "id_sistema")
	private Integer idSistema;

	/**
	 * nombre del sistema.
	 */
	@Column(name = "nombre")
	private String nombre;

	/** The cant preg desafio. */
	@Column(name = "cant_preg_desafio")
	private Integer cant_preg_desafio;

	/** The dias cambio clave. */
	@Column(name = "dias_cambio_clave")
	private Integer dias_cambio_clave;

	/**
	 * descripcion del tipo de proyecto.
	 */
	@Column(name = "descripcion")
	private String descripcion;

	/**
	 * Gets the id sistema.
	 *
	 * @return the id sistema
	 */
	public Integer getIdSistema() {
		return idSistema;
	}

	/**
	 * Sets the id sistema.
	 *
	 * @param idSistema the new id sistema
	 */
	public void setIdSistema(Integer idSistema) {
		this.idSistema = idSistema;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the cant preg desafio.
	 *
	 * @return the cant preg desafio
	 */
	public Integer getCant_preg_desafio() {
		return cant_preg_desafio;
	}

	/**
	 * Sets the cant preg desafio.
	 *
	 * @param cant_preg_desafio the new cant preg desafio
	 */
	public void setCant_preg_desafio(Integer cant_preg_desafio) {
		this.cant_preg_desafio = cant_preg_desafio;
	}

	/**
	 * Gets the dias cambio clave.
	 *
	 * @return the dias cambio clave
	 */
	public Integer getDias_cambio_clave() {
		return dias_cambio_clave;
	}

	/**
	 * Sets the dias cambio clave.
	 *
	 * @param dias_cambio_clave the new dias cambio clave
	 */
	public void setDias_cambio_clave(Integer dias_cambio_clave) {
		this.dias_cambio_clave = dias_cambio_clave;
	}
}
