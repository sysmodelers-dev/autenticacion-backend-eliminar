package com.sysmodelers.autenticacion.data.model;

// TODO: Auto-generated Javadoc
/**
 * The Class UsuarioLogin.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 */
public class UsuarioLogin {

	/** The sistema. */
	private String sistema;

	/** The login. */
	private String login;

	/** The password. */
	private String password;

	/**
	 * Gets the sistema.
	 *
	 * @return the sistema
	 */
	public String getSistema() {
		return sistema;
	}

	/**
	 * Sets the sistema.
	 *
	 * @param sistema the new sistema
	 */
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login.
	 *
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
