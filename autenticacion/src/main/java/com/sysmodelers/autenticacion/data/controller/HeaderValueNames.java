/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller;

/**
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
public enum HeaderValueNames {

	/** Id del Token de la sesion del usuario */
	HEADER_AUTH_TOKEN("autenticacion-token"),

	/** ID del usuario haciendo la solicitud. */
	HEADER_USER_ID("autenticacion-user-id"),

	/** Password del usuario en el repositorio */
	HEADER_USER_KEY("autenticacion-user-key"); 

	/** */
	private String h;

	/**
	 * 
	 * @param t
	 */
	HeaderValueNames(String h) {
		this.h = h;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return this.h;
	}

	/**
	 * 
	 * @param t
	 * @return
	 */
	public static HeaderValueNames fromString(String t) {
		if (t != null)
			for (HeaderValueNames h_ : HeaderValueNames.values())
				if (t.equalsIgnoreCase(h_.h))
					return h_;
		throw new IllegalArgumentException("No constant with text " + t + " found");
	}

}
