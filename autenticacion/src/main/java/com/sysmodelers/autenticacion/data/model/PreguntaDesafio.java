package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class PreguntaDesafio.
 */
@Entity
@Table(name = "pregunta_desafio")
@SequenceGenerator(name = "seq_pregunta_desafio_id_pregunta_seq", sequenceName = "pregunta_desafio_id_pregunta_seq", allocationSize = 1)
public class PreguntaDesafio implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7553902955421954340L;

	/**
	 * Identificador del usuario.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pregunta_desafio_id_pregunta_seq")
	@Column(name = "id_pregunta")
	private Integer idPregunta;

	/**
	 * descripcion del tipo de proyecto.
	 */
	@Column(name = "descripcion")
	private String descripcion;

	/**
	 * Gets the id pregunta.
	 *
	 * @return the id pregunta
	 */
	public Integer getIdPregunta() {
		return idPregunta;
	}

	/**
	 * Sets the id pregunta.
	 *
	 * @param idPregunta the new id pregunta
	 */
	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
