package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * The Class AtributoProyecto.
 */
@Embeddable
public class Acceso implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -802661838489196363L;

	/** id rol. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_rol")
	@NotFound(action = NotFoundAction.IGNORE)
	private Rol rol;

	/**
	 * Gets the rol.
	 *
	 * @return the rol
	 */
	public Rol getRol() {
		return rol;
	}

	/**
	 * Sets the rol.
	 *
	 * @param rol the new rol
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}
}