/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model.util;

/**
 * Tipos de areas
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
public enum TipoElemento {

	PROYECTO("PROYECTO"), //
	TRABAJO("TRABAJO"), //
	TAREA("TAREA"), //
	ACTIVIDAD("ACTIVIDAD");

	/** ID. */
	private String tipo;

	/**
	 * default constructor.
	 */
	private TipoElemento(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {

		switch (tipo) {
		case "PROYECTO":
			return "PROYECTO";
		case "TRABAJO":
			return "TRABAJO";
		case "TAREA":
			return "TAREA";
		case "ACTIVIDAD":
			return "ACTIVIDAD";
		default:
			return null;
		}

	}

	/**
	 * 
	 * @param tipo
	 * @return
	 */
	public static TipoElemento fromVal(String tipo) {
		for (TipoElemento temp : TipoElemento.values()) {
			if (tipo == temp.tipo) {
				return temp;
			}
		}
		return null;
	}

}
