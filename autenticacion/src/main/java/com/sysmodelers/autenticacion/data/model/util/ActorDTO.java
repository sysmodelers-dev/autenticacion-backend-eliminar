/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model.util;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorDTO.
 */
public class ActorDTO {

	/** tipo del elemento 0:PROYECTO, 1:TRABAJO, 2:TAREA, 3:ACTIVIDAD. */
	@Enumerated(EnumType.STRING)
	private TipoElemento tipoElemento;

	/** The id elemento. */
	private Integer idElemento;

	/** The id usuario. */
	private Integer idUsuario;

	/** tipo del actor RESPONSABLE, INFORMADOR, APROBADOR. */
	@Enumerated(EnumType.STRING)
	private TipoActor tipoActor;

	/**
	 * Gets the tipo elemento.
	 *
	 * @return the tipo elemento
	 */
	public TipoElemento getTipoElemento() {
		return tipoElemento;
	}

	/**
	 * Sets the tipo elemento.
	 *
	 * @param tipoElemento the new tipo elemento
	 */
	public void setTipoElemento(TipoElemento tipoElemento) {
		this.tipoElemento = tipoElemento;
	}

	/**
	 * Gets the id elemento.
	 *
	 * @return the id elemento
	 */
	public Integer getIdElemento() {
		return idElemento;
	}

	/**
	 * Sets the id elemento.
	 *
	 * @param idElemento the new id elemento
	 */
	public void setIdElemento(Integer idElemento) {
		this.idElemento = idElemento;
	}

	/**
	 * Gets the id usuario.
	 *
	 * @return the id usuario
	 */
	public Integer getIdUsuario() {
		return idUsuario;
	}

	/**
	 * Sets the id usuario.
	 *
	 * @param idUsuario the new id usuario
	 */
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * Gets the tipo actor.
	 *
	 * @return the tipo actor
	 */
	public TipoActor getTipoActor() {
		return tipoActor;
	}

	/**
	 * Sets the tipo actor.
	 *
	 * @param tipoActor the new tipo actor
	 */
	public void setTipoActor(TipoActor tipoActor) {
		this.tipoActor = tipoActor;
	}

}
