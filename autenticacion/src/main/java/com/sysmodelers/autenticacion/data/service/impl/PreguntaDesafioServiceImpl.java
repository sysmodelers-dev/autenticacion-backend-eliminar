/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sysmodelers.autenticacion.data.dao.EntityDAO;
import com.sysmodelers.autenticacion.data.model.PreguntaDesafio;
import com.sysmodelers.autenticacion.data.service.ManagerImpl;

/**
 * Manejador en base de datos de objetos {@link PreguntaDesafio}.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
@Service("preguntaDesafioService")
public class PreguntaDesafioServiceImpl extends ManagerImpl<PreguntaDesafio> {

	/** Interfaz a base de datos. */
	@Autowired
	@Qualifier("preguntaDesafioDao")
	public void setDao(EntityDAO<PreguntaDesafio> dao) {
		super.setDao(dao);
	}

}
