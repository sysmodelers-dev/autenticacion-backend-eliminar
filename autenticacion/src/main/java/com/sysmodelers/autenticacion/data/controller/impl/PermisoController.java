/**
* Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
*/
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sysmodelers.autenticacion.data.controller.CustomRestController;
import com.sysmodelers.autenticacion.data.controller.RESTController;
import com.sysmodelers.autenticacion.data.controller.util.EscapedLikeRestrictions;
import com.sysmodelers.autenticacion.data.model.Permiso;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * Controladores REST para manejo de elementos tipo
 * {@link com.Permiso.sigap.data.model.Permiso}
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@RestController
public class PermisoController extends CustomRestController implements RESTController<Permiso> {

	/** Log de suscesos. */
	private static final Logger log = Logger.getLogger(PermisoController.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#get(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/permiso", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Permiso> get(@RequestParam(value = "id", required = true) Serializable id) {

		Permiso item = null;
		try {

			item = mngrPermiso.fetch(Integer.valueOf((String) id));

			log.debug(item);

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		log.debug(" Item Out >> " + item);
		return new ResponseEntity<Permiso>(item, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#search(java.lang.Object)
	 */
	@Override
	@RequestMapping(value = "/permiso", method = RequestMethod.POST)
	public ResponseEntity<List<?>> search(@RequestBody(required = true) Permiso permiso) {

		List<?> lst = new ArrayList<Permiso>();
		log.debug("PARAMETROS DE BUSQUEDA : " + permiso);

		try {

			// * * * * * * * * * * * * * * * * * * * * * *
			List<Criterion> restrictions = new ArrayList<Criterion>();

			if (permiso.getIdPermiso() != null)
				restrictions.add(Restrictions.idEq(permiso.getIdPermiso()));

			if (StringUtils.isNotBlank(permiso.getDescripcion()))
				restrictions.add(
						EscapedLikeRestrictions.ilike("descripcion", permiso.getDescripcion(), MatchMode.ANYWHERE));

			if (StringUtils.isNotBlank(permiso.getNombreCorto()))
				restrictions.add(
						EscapedLikeRestrictions.ilike("nombre_corto", permiso.getNombreCorto(), MatchMode.ANYWHERE));

			if (permiso.getSistema() != null)
				if (permiso.getSistema().getIdSistema() != null)
					restrictions.add(Restrictions.eq("sistema.idSistema", permiso.getSistema().getIdSistema()));

			List<Order> orders = new ArrayList<Order>();

			orders.add(Order.asc("descripcion"));

			// * * * * * * * * * * * * * * * * * * * * * *
			lst = mngrPermiso.search(restrictions, orders);

			log.debug("Size found >> " + lst.size());

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		return new ResponseEntity<List<?>>(lst, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#save(java.lang.Object)
	 */
	// Parametros para ser usados por Swagger
	@ApiImplicitParams({
			@ApiImplicitParam(name = "autenticacion-user-id", required = true, dataType = "string", paramType = "header") })
	@Override
	@RequestMapping(value = "/permiso", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Permiso> save(@RequestBody(required = true) Permiso permiso) throws Exception {

		try {

			log.debug("Permiso A GUARDAR >> " + permiso);

			if (permiso.getIdPermiso() == null) {
				mngrPermiso.save(permiso);
				Permiso permiso_ = mngrPermiso.fetch(permiso.getIdPermiso());
				return new ResponseEntity<Permiso>(permiso_, HttpStatus.CREATED);
			} else {
				mngrPermiso.update(permiso);
				return new ResponseEntity<Permiso>(permiso, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ecm.sigap.data.controller.RestController#delete(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/permiso", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@RequestParam(value = "id", required = true) Serializable id) {

		log.debug("permiso A ELIMINAR >> " + id);

		try {
			mngrPermiso.delete(mngrPermiso.fetch(Integer.valueOf((String) id)));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}
}