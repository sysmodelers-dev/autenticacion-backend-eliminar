/**
f * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller;

import java.time.Duration;
import java.time.Instant;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.sysmodelers.autenticacion.data.model.Permiso;
import com.sysmodelers.autenticacion.data.model.PreguntaDesafio;
import com.sysmodelers.autenticacion.data.model.Rol;
import com.sysmodelers.autenticacion.data.model.Sistema;
import com.sysmodelers.autenticacion.data.model.Usuario;
import com.sysmodelers.autenticacion.data.service.EntityManager;
import com.sysmodelers.autenticacion.security.util.Security;

/**
 * Arquetipo de un controlador REST.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 */
public abstract class CustomRestController {

	/**
	 * Contexto Spring de la aplicacion.
	 */
	private static ApplicationContext appContext;

	/**
	 * Log de suscesos.
	 */
	private static final Logger log = Logger.getLogger(CustomRestController.class);

	// /** */
	// protected static final ResourceBundle errorMessages =
	// ResourceBundle.getBundle("errorMessages");

	/**
	 * Configuracion global de la acplicacion.
	 */
	@Autowired
	protected Environment environment;

	/**
	 * Manejador para el tipo {@link com.Usuario.archivo.data.model.Usuario}
	 */
	@Autowired
	@Qualifier("usuarioService")
	protected EntityManager<Usuario> mngrUsuario;

	/**
	 * Manejador para el tipo
	 * {@link com.sysmodelers.autenticacion.data.model.PreguntaDesafio}
	 */
	@Autowired
	@Qualifier("preguntaDesafioService")
	protected EntityManager<PreguntaDesafio> mngrPreguntaDesafio;

	/**
	 * Manejador para el tipo
	 * {@link com.sysmodelers.autenticacion.data.model.Sistema}
	 */
	@Autowired
	@Qualifier("sistemaService")
	protected EntityManager<Sistema> mngrSistema;

	/**
	 * Manejador para el tipo {@link com.sysmodelers.autenticacion.data.model.Rol}
	 */
	@Autowired
	@Qualifier("rolService")
	protected EntityManager<Rol> mngrRol;
	
	/**
	 * Manejador para el tipo {@link com.sysmodelers.autenticacion.data.model.Permiso}
	 */
	@Autowired
	@Qualifier("permisoService")
	protected EntityManager<Permiso> mngrPermiso;

	/**
	 * Solicitud http.
	 */
	@Autowired
	private HttpServletRequest request;

	/**
	 * Default Constructor.
	 */
	public CustomRestController() {
		super();
	}

	/**
	 * Obtiene la instancia de contexto de la aplicacion.
	 *
	 * @return the appContext
	 */
	public ApplicationContext getAppContext() {
		return appContext;
	}

	/**
	 * Spring {@link Environment}
	 *
	 * @return
	 */
	protected Environment getEnvironment() {
		return environment;
	}

	/**
	 * Desencripta el valor que se pase por parametro
	 *
	 * @param value Valor a ser desencriptado
	 * @return Valor desencriptado
	 */
	protected String decryptText(String value) {

		try {

			return Security.decript(value);

		} catch (Exception e) {
			log.error("Error al momento de desencriptar el valor con la siguiente descripcrion: " + e.getMessage());
			e.printStackTrace();
			return value;
		}
	}

	protected String encryptText(String value) {

		try {

			return Security.encript(value);

		} catch (Exception e) {
			log.error("Error al momento de encriptar el valor con la siguiente descripcrion: " + e.getMessage());
			e.printStackTrace();
			return value;
		}
	}

	/**
	 * Obtiene el valor del header inidicado.
	 *
	 * @param headerName Nombre del atributo que se desea obtener de la cabecera
	 * @param decrypted  Opcion para desencriptar o no el valor que se retorna
	 * @return Valor del header inidicado
	 */
	protected String getHeader(HeaderValueNames headerName, boolean decrypted) {

		String value = request.getHeader(headerName.getName());

		log.debug(headerName.toString() + " >> " + value);

		if (decrypted) {
			return decryptText(value);
		}

		return value;
	}

	/**
	 * Obtiene el valor del header inidicado.
	 *
	 * @param headerName Nombre del atributo que se desea obtener de la cabecera
	 * @return Valor del header inidicado
	 */
	protected String getHeader(HeaderValueNames headerName) {
		return getHeader(headerName, true);
	}

	/**
	 * Obtiene la direccion IP del equipo que esta haciendo la peticion
	 *
	 * @return Direccion IP del equipo que esta haciendo la peticion
	 */
	protected String getRemoteIpAddress() {

		String ipAddress = request.getHeader("X-FORWARDED-FOR");

		return (ipAddress != null) ? ipAddress : request.getRemoteAddr();
	}

	public void timeElapsed(Instant start, String msg) {
		log.debug(":::: Time elapsed for " + msg + " event: " + Duration.between(start, Instant.now()));
	}

}