/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Ajusta el valos "S" o "Y" a un valor booleano.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@Converter
public class BooleanToStringConverter implements
		AttributeConverter<Boolean, String> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang
	 * .Object)
	 */
	@Override
	public String convertToDatabaseColumn(Boolean value) {
		return (value != null && value) ? "S" : "N";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang
	 * .Object)
	 */
	@Override
	public Boolean convertToEntityAttribute(String value) {
		return "Y".equalsIgnoreCase(value) || "S".equalsIgnoreCase(value)
				|| "YES".equalsIgnoreCase(value)
				|| "SI".equalsIgnoreCase(value);
	}
}