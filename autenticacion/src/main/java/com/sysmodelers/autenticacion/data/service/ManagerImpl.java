/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.service;

import com.sysmodelers.autenticacion.data.dao.EntityDAO;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Cableado entre los metodos expuiestos del manager y los metodos entregados
 * por el DAO.
 *
 * @param <k> Tipo de Objetos que el Manager utilizara.
 * @author Adaulfo Herrera
 * @version 1.0
 */
public abstract class ManagerImpl<k> implements EntityManager<k> {

    /**
     * Configuracion global de la acplicacion.
     */
    @Autowired
    protected Environment environment;

    /**
     * Interfaz a base de datos.
     */
    private EntityDAO<k> dao;

    @Autowired
    protected PlatformTransactionManager transactionManager;
    /** */
    private TransactionStatus transactionStatus;

    /**
     * Default Constructor.
     */
    public ManagerImpl() {
        super();
    }

    /**
     * @param dao
     */
    protected void setDao(EntityDAO<k> dao) {
        this.dao = dao;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#fetch(java.io.Serializable)
     */
    @Override
    public k fetch(Serializable id) {
        return dao.fetch(id);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#save(java.lang.Object)
     */
    @Override
    public void save(k item) throws Exception {
        dao.save(item);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#update(java.lang.Object)
     */
    @Override
    public void update(k item) {
        dao.update(item);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#delete(java.lang.Object)
     */
    @Override
    public void delete(k item) {
        dao.delete(item);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#fetchAll()
     */
    @Override
    public List<k> fetchAll() {
        return dao.fetchAll();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ecm.archivo.data.service.EntityManager#search(org.hibernate.criterion
     * .Criterion, org.hibernate.criterion.Order)
     */
    @Override
    public List<?> search(List<Criterion> restrictions, List<Order> orders, ProjectionList projections,
                          Integer fetchSize, Integer firstResult) {
        return dao.search(restrictions, orders, projections, fetchSize, firstResult);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#search(java.util.List)
     */
    @Override
    public List<?> search(List<Criterion> restrictions) {
        return dao.search(restrictions, null, null, null, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#search(java.util.List,
     * java.util.List)
     */
    @Override
    public List<?> search(List<Criterion> restrictions, List<Order> orders) {
        return dao.search(restrictions, orders, null, null, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ecm.archivo.data.service.EntityManager#execNamedQuery(java.lang.String,
     * java.util.HashMap)
     */
    @Override
    public List<k> execNamedQuery(String queryName, HashMap<String, Object> params) {
        return dao.execNamedQuery(queryName, params);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#execUpdateQuery(java.lang.
     * String, java.util.HashMap)
     */
    @Override
    public Integer execUpdateQuery(String queryName, HashMap<String, Object> params) {
        return dao.execUpdateQuery(queryName, params);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.ecm.archivo.data.service.EntityManager#uniqueResult(java.lang.String,
     * java.util.HashMap)
     */
    @Override
    public Object uniqueResult(String queryName, HashMap<String, Object> params) {
        return dao.uniqueResult(queryName, params);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#isConnected()
     */
    @Override
    public String isConnected() {
        return dao.isConnected();
    }


    /*
     *
     */
    @Override
    public Long getNextval(String seq) {
        return dao.getNextval(seq);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#flush()
     */
    @Override
    public void flush() {
        dao.flush();
    }

    /*
 * (non-Javadoc)
 *
 * @see com.ecm.archivo.data.service.EntityManager#beginTransaction()
 */
    @Override
    public void beginTransaction() {
        TransactionDefinition def = new DefaultTransactionDefinition();
        transactionStatus = transactionManager.getTransaction(def);

    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#commit()
     */
    @Override
    public void commit() {
        if (transactionStatus != null)
            transactionManager.commit(transactionStatus);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ecm.archivo.data.service.EntityManager#rollback()
     */
    @Override
    public void rollback() {
        if (transactionStatus != null)
            transactionManager.rollback(transactionStatus);

    }

}
