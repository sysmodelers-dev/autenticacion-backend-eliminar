/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sysmodelers.autenticacion.data.dao.EntityDAO;
import com.sysmodelers.autenticacion.data.model.Sistema;
import com.sysmodelers.autenticacion.data.service.ManagerImpl;

/**
 * Manejador en base de datos de objetos {@link Sistema}.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 * 
 */
@Service("sistemaService")
public class SistemaServiceImpl extends ManagerImpl<Sistema> {

	/** Interfaz a base de datos. */
	@Autowired
	@Qualifier("sistemaDao")
	public void setDao(EntityDAO<Sistema> dao) {
		super.setDao(dao);
	}

}
