/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * Clase de entidad que representa a los Usuario Internos del Sistema.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 */
@Entity
@Table(name = "usuario")
@SequenceGenerator(name = "seq_usuario_id_usuario_seq", sequenceName = "usuario_id_usuario_seq", allocationSize = 1)
public class Usuario implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9150301796968429649L;

	/**
	 * Identificador del usuario.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario_id_usuario_seq")
	@Column(name = "id_usuario")
	private Integer idUsuario;

	/** Sistema al que pertenece el usuario. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sistema")
	@NotFound(action = NotFoundAction.IGNORE)
	private Sistema sistema;

	/** id rol. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_rol")
	@NotFound(action = NotFoundAction.IGNORE)
	private Rol rol;

	/** login_usuario. */
	@Column(name = "login_usuario")
	private String loginUsuario;

	/**
	 * Contraseña encriptada por la fiel.
	 */
	@Column(name = "password")
	private String password;

	/**
	 * nombre del usuario.
	 */
	@Column(name = "nombre")
	private String nombre;

	/**
	 * email del usuariol.
	 */
	@Column(name = "e_mail")
	private String email;

	/** usuario activo ?. */
	@Column(name = "activo")
	private boolean activo;

	/** preguntas de desafio del usuario. */
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "usuario_pregunta_desafio", joinColumns = { @JoinColumn(name = "id_usuario") })
	@JoinColumn(name = "id_usuario")
	@Fetch(value = FetchMode.SUBSELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<UsuarioPreguntaDesafio> usuarioPreguntaDesafio;

	/** Roles de cada usuario. */
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "acceso", joinColumns = { @JoinColumn(name = "id_usuario") })
	@JoinColumn(name = "id_usuario")
	@Fetch(value = FetchMode.SUBSELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<Acceso> acceso;

	/**
	 * Gets the id usuario.
	 *
	 * @return the id usuario
	 */
	public Integer getIdUsuario() {
		return idUsuario;
	}

	/**
	 * Sets the id usuario.
	 *
	 * @param idUsuario the new id usuario
	 */
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * Gets the login usuario.
	 *
	 * @return the login usuario
	 */
	public String getLoginUsuario() {
		return loginUsuario;
	}

	/**
	 * Sets the login usuario.
	 *
	 * @param loginUsuario the new login usuario
	 */
	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the rol.
	 *
	 * @return the rol
	 */
	public Rol getRol() {
		return rol;
	}

	/**
	 * Sets the rol.
	 *
	 * @param rol the new rol
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Checks if is activo.
	 *
	 * @return true, if is activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * Sets the activo.
	 *
	 * @param activo the new activo
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * Gets the usuario pregunta desafio.
	 *
	 * @return the usuario pregunta desafio
	 */
	public List<UsuarioPreguntaDesafio> getUsuarioPreguntaDesafio() {
		return usuarioPreguntaDesafio;
	}

	/**
	 * Sets the usuario pregunta desafio.
	 *
	 * @param usuarioPreguntaDesafio the new usuario pregunta desafio
	 */
	public void setUsuarioPreguntaDesafio(List<UsuarioPreguntaDesafio> usuarioPreguntaDesafio) {
		this.usuarioPreguntaDesafio = usuarioPreguntaDesafio;
	}

	/**
	 * Gets the sistema.
	 *
	 * @return the sistema
	 */
	public Sistema getSistema() {
		return sistema;
	}

	/**
	 * Sets the sistema.
	 *
	 * @param sistema the new sistema
	 */
	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	/**
	 * Gets the acceso.
	 *
	 * @return the acceso
	 */
	public List<Acceso> getAcceso() {
		return acceso;
	}

	/**
	 * Sets the acceso.
	 *
	 * @param acceso the new acceso
	 */
	public void setAcceso(List<Acceso> acceso) {
		this.acceso = acceso;
	}

}