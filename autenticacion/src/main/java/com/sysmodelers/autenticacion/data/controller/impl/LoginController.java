/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sysmodelers.autenticacion.data.controller.CustomRestController;
import com.sysmodelers.autenticacion.data.model.Usuario;
import com.sysmodelers.autenticacion.data.model.UsuarioLogin;
import com.sysmodelers.autenticacion.security.auth.RestClient;

/**
 * Controlador para la autenticacion del Usuario
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@RestController
public class LoginController extends CustomRestController {

	/** Log de suscesos. */
	private Logger log = Logger.getLogger(LoginController.class);

	/** URL del servicio de autenticacion de CAS */
	@Value("${seguridad.cas.url}")
	private String casUrl;

	private RestClient restClient;

	/**
	 * 
	 * Obtiene el token de la sesion del Usuario
	 * 
	 * @param usuarioLogin Informacion del usuario (Usuario y password)
	 * @return Token de la sesion del Usuario
	 */
	@RequestMapping(value = "/seguridad/login", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> loginUser(@RequestBody UsuarioLogin usuarioLogin) {

		log.debug("::: Iniciando el login del usuario(" + usuarioLogin + ")");

		String ticket = "";
		Map<String, String> response = new HashMap<String, String>();
		restClient = new RestClient();

		try {

			// Se obtienen los datos del usuario a partir de su Identificador
			Usuario usuario = null;
			List<Criterion> restrictions = new ArrayList<Criterion>();
//			restrictions.add(Restrictions.eq("sistema.idSistema", decryptText(usuarioLogin.getSistema())).ignoreCase());
//			restrictions.add(Restrictions.eq("loginUsuario", decryptText(usuarioLogin.getLogin())).ignoreCase());
			restrictions.add(Restrictions.eq("sistema.idSistema", Integer.parseInt(usuarioLogin.getSistema())));
			restrictions.add(Restrictions.eq("loginUsuario", usuarioLogin.getLogin()).ignoreCase());

			List<?> items = mngrUsuario.search(restrictions);

			if (null == items || items.isEmpty()) {

				log.error("Error al momento de ejecutar la autenticacion - Usuario '" + usuarioLogin.getLogin()
						+ "' no existe dentro del sistema ");
				response.put("error",
						"Error al momento de ejecutar la autenticacion - Usuario y/o password incorrectos");
				// No se encontro el usuario por lo que se niega el acceso
				return new ResponseEntity<Map<String, String>>(response, HttpStatus.PRECONDITION_FAILED);
			}

			usuario = (Usuario) items.get(0);
			log.debug("::: Usuario >>" + usuario);

			// ********************************************************************
//				String location = getTicketGrantingTicket(usuarioLogin.getLogin(), usuarioLogin.getPassword());
//
//				log.debug("::: Location= " + location);
//				// get SGT
//				ticket = getServiceGrantingTicket(location, casUrl);
			// ********************************************************************

			if (!usuario.getPassword().equals(usuarioLogin.getPassword()))
				response.put("error",
						"Error al momento de ejecutar la autenticacion - Usuario y/o password incorrectos");

			log.debug("::: Ticket= " + ticket);
			response.put("aceptoPolitica", String.valueOf(true));
			response.put("ticket", ticket);
			response.put("sistema", String.valueOf(usuario.getSistema().getIdSistema()));
			response.put("idUsuario", usuario.getLoginUsuario().toString());
			response.put("accessToken", generateAccessToken(usuarioLogin.getSistema(), usuarioLogin.getLogin(),
					usuarioLogin.getPassword()));

			return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);

		} catch (

		Exception e) {
			log.error("::: Se genero un error en el metodo con la siguiente descripcion: " + e.getMessage());
			e.printStackTrace();

		}
		response.put("error", "Error al momento de ejecutar la autenticacion - Usuario y/o password incorrectos");
		return new ResponseEntity<Map<String, String>>(response, HttpStatus.PRECONDITION_FAILED);
	}

	/**
	 * With the TGT location and service url this will get the SGT
	 * 
	 * @param tgtLocation
	 * @param serviceUrl
	 * @return
	 * @throws IOException
	 */
	private String getServiceGrantingTicket(String tgtLocation, String serviceUrl) throws IOException {
		log.debug("::: Se va a obtener el service ticket del ticket " + tgtLocation);
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("service", serviceUrl);
		params.put("method", "POST");

		HttpURLConnection conn = restClient.post(tgtLocation, params);
		StringBuilder responseBuilder = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		String input;
		while ((input = in.readLine()) != null) {
			responseBuilder.append(input);
		}
		in.close();

		String response = responseBuilder.toString();
		log.debug("SGT -> " + response);

		return response;
	}

	/**
	 * Gets the TGT for the given username and password
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 */
	private String getTicketGrantingTicket(String username, String password) throws IOException {
		log.debug("::: Se va a obtener el ticket para el usuario " + username);
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("username", username);
		params.put("password", password);
		params.put("option", "LoginControl");

		HttpURLConnection conn = restClient.post(casUrl, params);

		log.debug(" conn.getResponseCode() " + conn.getResponseCode());

		if (conn.getResponseCode() == 400) {
			// throw new AuthenticationException("bad username or password");
			throw new IOException("bad username or password");
		}
		String location = conn.getHeaderField("Location");
		log.debug("TGT LOCATION -> " + location);
		return location;
	}

	/**
	 * 
	 * Actualiza la aceptacion de las policitas de uso del sistema del Usuario
	 * 
	 * @param usuarioLogin Informacion del usuario (Usuario) que acepta la politica
	 * @return Token de la sesion del Usuario
	 */
	@RequestMapping(value = "/seguridad/politicasuso", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Map<String, String>> aceptaPoliticas(
			@RequestParam(value = "id", required = true) Serializable id) throws Exception {

		log.debug("::: Usuario acepto las politicas de uso (" + id + ")");

		Map<String, String> response = new HashMap<String, String>();

		try {
			// Se obtienen los datos del usuario a partir de su Identificador
			Usuario usuario = null;
			List<Criterion> restrictions = new ArrayList<Criterion>();
			restrictions.add(Restrictions.eq("userKey", id).ignoreCase());

			List<?> items = mngrUsuario.search(restrictions);

			if (null != items && !items.isEmpty()) {
				usuario = (Usuario) items.get(0);

				log.debug("::: Usuario >> " + usuario + " acepto las politicas de uso");
				String ipAddress = getRemoteIpAddress();
				log.debug("IP remota " + ipAddress);

				// UsuarioCapacita usuarioCapacita =
				// mngrUsuarioCapacita.fetch(usuario.getIdUsuario());
				// usuarioCapacita.setAcepto(Boolean.TRUE);
				// usuarioCapacita.setFecha(new Date());
				// usuarioCapacita.setIp(ipAddress);
				//
				// response.put("aceptado", "true");
				// mngrUsuarioCapacita.update(usuarioCapacita);

				return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);

			} else {

				log.error("Error al momento aceptar las politicas de uso del usuario " + id + ". Usuario no existe");
				response.put("aceptado", "false");
				// No se encontro el usuario por lo que se niega el acceso
				return new ResponseEntity<Map<String, String>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			log.error("::: Se genero un error en el metodo con la siguiente descripcion: " + e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}

	private String generateAccessToken(String sistema, String user, String pass) {

		String accessToken = String.format("%s$$#$$%s$$#$$%s$$#$$%d", sistema, user, pass, System.currentTimeMillis());
		return encryptText(accessToken);
	}

	/**
	 *
	 * @param body
	 * @return
	 */
	@RequestMapping(value = "/seguridad/access", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> loginUserWithAccessToken(
			@RequestBody Map<String, String> body) {
		UsuarioLogin login = new UsuarioLogin();
		Map<String, String> error = new HashMap<>();
		try {

			String token[] = decryptText(body.get("accessToken")).split("(\\$\\$\\#\\$\\$)");
			LocalDateTime dateTime = Instant.ofEpochMilli(Long.parseLong(token[3])).atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			long min = ChronoUnit.MINUTES.between(dateTime, LocalDateTime.now());
			if (min > 30) {
				error.put("error", "El token a expirado");
				return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
			}
			login.setSistema(token[0]);
			login.setLogin(token[1]);
			login.setPassword(token[2]);
		} catch (Exception ex) {
			error.put("error", "Invalid access token");
			return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
		}
		return loginUser(login);
	}
}