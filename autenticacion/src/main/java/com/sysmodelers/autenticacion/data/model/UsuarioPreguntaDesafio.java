package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * The Class AtributoProyecto.
 */
@Embeddable
public class UsuarioPreguntaDesafio implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8953990752804852430L;
	
	/** id pregunta. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pregunta")
	@NotFound(action = NotFoundAction.IGNORE)
	private PreguntaDesafio preguntaDesafio;

	/** respuesta pregunta desafio. */
	@Column(name = "respuesta")
	private String respuesta;
	
	/**
	 * Gets the pregunta desafio.
	 *
	 * @return the pregunta desafio
	 */
	public PreguntaDesafio getPreguntaDesafio() {
		return preguntaDesafio;
	}

	/**
	 * Sets the pregunta desafio.
	 *
	 * @param preguntaDesafio the new pregunta desafio
	 */
	public void setPreguntaDesafio(PreguntaDesafio preguntaDesafio) {
		this.preguntaDesafio = preguntaDesafio;
	}

	/**
	 * Gets the respuesta.
	 *
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * Sets the respuesta.
	 *
	 * @param respuesta the new respuesta
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

}
