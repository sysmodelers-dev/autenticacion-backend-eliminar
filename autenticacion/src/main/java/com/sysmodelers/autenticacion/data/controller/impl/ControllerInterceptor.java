/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones Avanzadas de ECM, S.A. de C.V. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Interceptor de todos los request hacia el core, filtra solicitudes
 * desconocidas de usuarios no autenticados en la applicacion
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
public class ControllerInterceptor extends HandlerInterceptorAdapter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
	 * preHandle(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		String requestURL = request.getRequestURI();

		if (requestURL.contains("/seguridad/") || "/autenticacion-core".equals(requestURL))
			return true;

		// String user_id =
		// request.getHeader(HeaderValueNames.HEADER_USER_ID.getName());

		// if (user_id == null)
		// throw new Exception("Header " +
		// HeaderValueNames.HEADER_USER_ID.getName() + " missing value!");
		//
		// String user_key =
		// request.getHeader(HeaderValueNames.HEADER_USER_KEY.getName());
		//
		// if (user_key == null)
		// throw new Exception("Header " +
		// HeaderValueNames.HEADER_USER_KEY.getName() + " missing value!");
		//
		// String area_id =
		// request.getHeader(HeaderValueNames.HEADER_AREA_ID.getName());
		//
		// if (area_id == null)
		// throw new Exception("Header " +
		// HeaderValueNames.HEADER_AREA_ID.getName() + " missing value!");
		//
		// String token_id =
		// request.getHeader(HeaderValueNames.HEADER_AUTH_TOKEN.getName());
		//
		// if (token_id == null)
		// throw new Exception("Header " +
		// HeaderValueNames.HEADER_AUTH_TOKEN.getName() + " missing value!");

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
	 * postHandle(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object,
	 * org.springframework.web.servlet.ModelAndView)
	 */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/YYYY HH:MM:SS");

		response.setHeader("autenticacion-response-time", sdf.format(now));

	}
}