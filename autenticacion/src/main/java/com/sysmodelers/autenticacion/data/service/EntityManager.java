/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.service;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Interfaces de operaciones disponibles para manejo de datos hacia base de
 * datos.
 *
 * @param <T> Tipo de objeto devuelto.
 * @author Adaulfo Herrera
 * @version 1.0
 */
public interface EntityManager<T> {

    /**
     * Obtener.
     *
     * @param id ID del objeto.
     * @return Objeto solicitado.
     */
    public T fetch(Serializable id);

    /**
     * Guardar.
     *
     * @param item Nuevo objeto a agregar.
     */
    public void save(T item) throws Exception;

    /**
     * Actualizar.
     *
     * @param item
     */
    public void update(T item);

    /**
     * Eliminar.
     *
     * @param item Objeto que se eliminara.
     */
    public void delete(T item);

    /**
     * Obtener todos.
     *
     * @return Lista de objetos.
     */
    public List<T> fetchAll();

    /**
     * Execute Named Query.
     *
     * @param queryName Nombre del query a ejecutar.
     * @param params    Parametros de query.
     * @return objetos devueltos por el query.
     */
    public List<T> execNamedQuery(String queryName, HashMap<String, Object> params);

    /**
     * Ejecuta un procedure/query retornando la cantidad de objetos afectados.
     *
     * @param queryName Nombre del query a ejecutar.
     * @param params    Parametros del query.
     * @return Cantidad de renglones afectados.
     */
    public Integer execUpdateQuery(String queryName, HashMap<String, Object> params);

    /**
     * Busqueda parametrizada.
     *
     * @param restrictions Lista de Parametros de busqueda.
     * @param orders       Lista de ordenes de la lista devueta.
     * @param fetchSize    Cantidad de elementos devueltos.
     * @param firstResult  Inidice del primer objeto devuelto.
     * @return Lista de Objetos devueltos en la busqueda.
     */
    @SuppressWarnings("rawtypes")
    public List search(List<Criterion> restrictions, List<Order> orders, ProjectionList projections, Integer fetchSize,
                       Integer firstResult);

    /**
     * Busqueda parametrizada.
     *
     * @param restrictions Lista de Parametros de busqueda.
     * @return Lista de Objetos devueltos en la busqueda.
     */
    public List<?> search(List<Criterion> restrictions);

    /**
     * Busqueda parametrizada.
     *
     * @param restrictions Lista de Parametros de busqueda.
     * @param orders       Lista de ordenes de la lista devueta.
     * @return Lista de Objetos devueltos en la busqueda.
     */
    public List<?> search(List<Criterion> restrictions, List<Order> orders);

    /**
     * @param queryName
     * @param params
     * @return
     */
    public Object uniqueResult(String queryName, HashMap<String, Object> params);

    /**
     * @return
     */
    public String isConnected();

    public Long getNextval(String seq);

    /**
     * Inicializa una transaccion
     */
    public void beginTransaction();

    /**
     * Realiza un commit a una transacccion existente
     */
    public void commit();

    /**
     * Realiza un rollback a una transacccion existente
     */
    public void rollback();

    /**
     * Refresca el cache del hibernate
     */
    public void flush();
}
