/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.dao;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Interfaces de operaciones disponibles para manejo de datos hacia base de
 * datos.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 *
 * @param <T>
 *            Tipo de objeto devuelto.
 */
public interface EntityDAO<T> {

	/**
	 * obtener.
	 *
	 * @param id
	 * @return
	 */
	public T fetch(Serializable id);

	/**
	 * guardar.
	 *
	 * @param item
	 */
	public void save(T item);

	/**
	 * actualizar.
	 *
	 * @param item
	 */
	public void update(T item);

	/**
	 * eliminar.
	 *
	 * @param item
	 */
	public void delete(T item);

	/**
	 * Refresca el cache del hibernate
	 */
	public void flush();

	/**
	 * obtener todos.
	 *
	 * @return
	 */
	public List<T> fetchAll();

	/**
	 * Execute Named Query.
	 *
	 * @param queryName
	 *            Nombre del Query.
	 * @param params
	 *            Parametros del Query.
	 * @return Lista de objectos devueltos por el query.
	 */
	public List<T> execNamedQuery(String queryName, HashMap<String, Object> params);

	/**
	 * Ejecutar un query por nombre.
	 *
	 * @param queryName
	 *            Nombre del query a ejecutar.
	 * @param params
	 *            PArametros del query.
	 * @return Numero de Renglones Afectados.
	 */
	public Integer execUpdateQuery(String queryName, HashMap<String, Object> params);

	/**
	 * Ejecutar un query por nombre que devuelve un valor unico.
	 *
	 * @param queryName
	 *            Nombre del query a ejecutar.
	 * @param params
	 *            Parametros del query.
	 * @return Valor unico devuelto por el query.
	 */
	public Object uniqueResult(String queryName, HashMap<String, Object> params);

	/**
	 * Busqueda parametrizada.
	 *
	 * @param restrictions
	 * @param orders
	 * @param fetchSize
	 * @param firstResult
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List search(List<Criterion> restrictions, List<Order> orders, ProjectionList projections, Integer fetchSize,
			Integer firstResult);

	/**
	 * Valida si la coneccion a base de datos esta activa.
	 *
	 * @return
	 */
	public String isConnected();

	/**
	 * Regresa la secuencia indicada
	 *
	 * @return
	 */
	public Long getNextval(String seq);
}
