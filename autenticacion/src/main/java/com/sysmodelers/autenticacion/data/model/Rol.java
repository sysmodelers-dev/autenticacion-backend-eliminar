/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * Clase de entidad que representa la tabla ROLES dentro del sistema.
 *
 * @author Adaulfo Herrera
 * @version 1.0
 */
@Entity
@Table(name = "rol")
@SequenceGenerator(name = "seq_rol_id_rol_seq", sequenceName = "rol_id_rol_seq", allocationSize = 1)
public class Rol implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6057342577878571414L;

	/** Identificador del Rol. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_rol_id_rol_seq")
	@Column(name = "id_rol")
	private Integer idRol;

	/** Sistema al que pertenece el usuario. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sistema")
	@NotFound(action = NotFoundAction.IGNORE)
	private Sistema sistema;

	/** Descripcion del Rol. */
	@Column(name = "descripcion")
	private String descripcion;

	/** permisos del rol */
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "rol_permiso", joinColumns = { @JoinColumn(name = "id_rol") })
	@JoinColumn(name = "id_rol")
	@Fetch(value = FetchMode.SUBSELECT)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<RolPermiso> rolPermiso;

	/**
	 * Gets the id rol.
	 *
	 * @return the id rol
	 */
	public Integer getIdRol() {
		return idRol;
	}

	/**
	 * Sets the id rol.
	 *
	 * @param idRol the new id rol
	 */
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	/**
	 * Gets the sistema.
	 *
	 * @return the sistema
	 */
	public Sistema getSistema() {
		return sistema;
	}

	/**
	 * Sets the sistema.
	 *
	 * @param sistema the new sistema
	 */
	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<RolPermiso> getRolPermiso() {
		return rolPermiso;
	}

	/**
	 * Sets the rol permiso.
	 *
	 * @param rolPermiso the new rol permiso
	 */
	public void setRolPermiso(List<RolPermiso> rolPermiso) {
		this.rolPermiso = rolPermiso;
	}

}
