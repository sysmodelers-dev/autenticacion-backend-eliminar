/**
 * Copyright (c) 2015 by Consultoria y Aplicaciones Avanzadas de ECM, S.A. de C.V. All Rights Reserved.
 */
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.apache.log4j.Logger;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Este controlador atrapa los error generados en los controladores REST y los
 * devuelve en manera de JSON.
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@ControllerAdvice
public class GlobalExceptionController {

	/** Log de suscesos. */
	private static final Logger LOGGER = Logger.getLogger(GlobalExceptionController.class);

	/**
	 * Handler para errores tipo {@link Exception}
	 * 
	 * @param ex Exception ocurrida durante la llamada de los servicios Rest
	 * @return Json que representa el error que se presento y el Codigo de Error
	 *         HTTP que representa dicho error
	 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseEntity<Map<String, String>> handleAllException(Exception ex) {

		Map<String, String> result = new HashMap<String, String>();

		// Se valida que la excepcion traiga una causa, de lo contrario se
		// coloca una generica
		if (null != ex.getCause()) {
			result.put("errorCause", ex.getCause().toString());
		} else {
			result.put("errorCause", "Error interno en el servidor");
		}

		result.put("statusText", ex.getMessage());

		if (ex.getClass() == GenericJDBCException.class) {
			result.put("sql", ((GenericJDBCException) ex).getSQL());
			result.put("sqlMessage", ((GenericJDBCException) ex).getSQLException().getMessage());
		}

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);

		result.put("stackTrace", sw.toString());

		if (ex instanceof MissingServletRequestParameterException) {
			// Exception del tipo 400 (Bad Request)
			LOGGER.error("::>> Los parametros requeridos por el metodo no fueron enviados completos");
			return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);

		} else if (ex instanceof HttpRequestMethodNotSupportedException) {

			// Exception del tipo 405 (Method Not Allowed)
			LOGGER.error("::>> El metodo que se quiere llamar no esta soportado");
			return new ResponseEntity<Map<String, String>>(result, HttpStatus.METHOD_NOT_ALLOWED);

		} else if (ex instanceof ConstraintViolationException) {

			// Exception del tipo 409 (Conflict)
			LOGGER.error("::>> Se violo alguna de las reglas de la entidad ");
			return new ResponseEntity<Map<String, String>>(result, HttpStatus.CONFLICT);
		} else if (ex instanceof JDBCConnectionException) {

			// Exception del tipo 503 (Service Unavailable)
			LOGGER.error("::>> Error al establecer la conexion con la base de datos ");
			return new ResponseEntity<Map<String, String>>(result, HttpStatus.SERVICE_UNAVAILABLE);
		} else if (ex instanceof AccessDeniedException) {

			// Exception del tipo 401
			return new ResponseEntity<Map<String, String>>(result, HttpStatus.UNAUTHORIZED);
		}

		// Exception del tipo 500 (Internal Server Error)
		return new ResponseEntity<Map<String, String>>(result, HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
