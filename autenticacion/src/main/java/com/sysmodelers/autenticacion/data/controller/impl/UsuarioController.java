/**
* Copyright (c) 2015 by Consultoria y Aplicaciones SysModelers. All Rights Reserved.
*/
package com.sysmodelers.autenticacion.data.controller.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sysmodelers.autenticacion.data.controller.CustomRestController;
import com.sysmodelers.autenticacion.data.controller.RESTController;
import com.sysmodelers.autenticacion.data.controller.util.EscapedLikeRestrictions;
import com.sysmodelers.autenticacion.data.model.Usuario;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * Controladores REST para manejo de elementos tipo
 * {@link com.Usuario.sigap.data.model.Usuario}
 * 
 * @author Adaulfo Herrera
 * @version 1.0
 *
 */
@RestController
public class UsuarioController extends CustomRestController implements RESTController<Usuario> {

	/** Log de suscesos. */
	private static final Logger log = Logger.getLogger(UsuarioController.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#get(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/usuario", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Usuario> get(@RequestParam(value = "id", required = true) Serializable id) {

		Usuario item = null;
		try {

			item = mngrUsuario.fetch(Integer.valueOf((String) id));

			log.debug(item);

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		log.debug(" Item Out >> " + item);
		return new ResponseEntity<Usuario>(item, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#search(java.lang.Object)
	 */
	@Override
	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public ResponseEntity<List<?>> search(@RequestBody(required = true) Usuario usuario) {

		List<?> lst = new ArrayList<Usuario>();
		log.debug("PARAMETROS DE BUSQUEDA : " + usuario);

		try {

			// * * * * * * * * * * * * * * * * * * * * * *
			List<Criterion> restrictions = new ArrayList<Criterion>();

			if (usuario.getIdUsuario() != null)
				restrictions.add(Restrictions.idEq(usuario.getIdUsuario()));

			if (StringUtils.isNotBlank(usuario.getNombre()))
				restrictions.add(EscapedLikeRestrictions.ilike("nombre", usuario.getNombre(), MatchMode.ANYWHERE));

			if (usuario.getSistema() != null)
				if (usuario.getSistema().getIdSistema() != null)
					restrictions.add(Restrictions.eq("sistema.idSistema", usuario.getSistema().getIdSistema()));

			List<Order> orders = new ArrayList<Order>();

			orders.add(Order.asc("nombre"));

			// * * * * * * * * * * * * * * * * * * * * * *
			lst = mngrUsuario.search(restrictions, orders);

			log.debug("Size found >> " + lst.size());

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}

		return new ResponseEntity<List<?>>(lst, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#save(java.lang.Object)
	 */
	// Parametros para ser usados por Swagger
	@ApiImplicitParams({
			@ApiImplicitParam(name = "autenticacion-user-id", required = true, dataType = "string", paramType = "header") })
	@Override
	@RequestMapping(value = "/usuario", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Usuario> save(@RequestBody(required = true) Usuario usuario) throws Exception {

		try {

			log.debug("Usuario A GUARDAR >> " + usuario);

			if (usuario.getIdUsuario() == null) {
				mngrUsuario.save(usuario);
				return new ResponseEntity<Usuario>(usuario, HttpStatus.CREATED);
			} else {
				mngrUsuario.update(usuario);
				return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ecm.sigap.data.controller.RESTController#save(java.lang.Object)
	 */
	// Parametros para ser usados por Swagger
	@ApiImplicitParams({
			@ApiImplicitParam(name = "autenticacion-user-id", required = true, dataType = "string", paramType = "header") })
	@RequestMapping(value = "/usuario", method = RequestMethod.PATCH)
	public @ResponseBody ResponseEntity<Usuario> actualizar(@RequestBody(required = true) Usuario usuario)
			throws Exception {

		try {

			log.debug("Usuario A GUARDAR >> " + usuario);

			if (usuario.getIdUsuario() == null) {
				mngrUsuario.save(usuario);
				return new ResponseEntity<Usuario>(usuario, HttpStatus.CREATED);
			} else {
				mngrUsuario.update(usuario);
				return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
			}

		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ecm.sigap.data.controller.RestController#delete(java.io.Serializable)
	 */
	@Override
	@RequestMapping(value = "/usuario", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@RequestParam(value = "id", required = true) Serializable id) {

		log.debug("usuario A ELIMINAR >> " + id);

		try {
			mngrUsuario.delete(mngrUsuario.fetch(Integer.valueOf((String) id)));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
	}
}