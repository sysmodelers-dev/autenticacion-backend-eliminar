package com.sysmodelers.autenticacion.data.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

// TODO: Auto-generated Javadoc
/**
 * The Class AtributoProyecto.
 */
@Embeddable
public class RolPermiso implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2349876142187869583L;

	/** id permiso. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_permiso")
	@NotFound(action = NotFoundAction.IGNORE)
	private Permiso permiso;

	/**
	 * Gets the permiso.
	 *
	 * @return the permiso
	 */
	public Permiso getPermiso() {
		return permiso;
	}

	/**
	 * Sets the permiso.
	 *
	 * @param permiso the new permiso
	 */
	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

}